
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.net.*;
import java.applet.AppletContext;

class Customer 
{
		protected String custid;
		protected String name;
		protected String address;
		protected String password;
		

		public Customer()
		{
			this("","","","");
		}
		public Customer(String cid,String nam,String addr,String pass)
		{
			custid=cid;
			name=nam;
			address=addr;
			password=pass;
		}
		
		public String getName()
		{
			return name;
		}
		String getPasswrd()
		{
			return password;
		}
		String getId()
		{
			return custid;
		}
		String getAddress()
		{
			return address;
		}
		
		void setId(String custid)
		{
			this.custid=custid;
		}
		
		void setName(String name)
		{
			this.name=name;
		}
		void setAddress(String address)
		{
			this.address=address;
		}
		void setPassword(String password)
		{
			this.password=password;
		}
		public String toString()
		{
			return custid+' '+name+' '+address+' '+password+' ';
		}
	 	
}	
class Account
{
	protected String custid;
	protected long amount;
	
	Account()
	{
		this("",0);
	}
	public Account(String custid,long amount)
	{
		this.custid=custid;
		this.amount=amount;
	}
	public void setId(String custid)
	{
		this.custid=custid;
	}
	public void setAmount(long amount)
	{
		this.amount=amount;
	}
	public long getAmount()
	{
		return amount;
	}
}

class AccountProcess extends Account
{
	private RandomAccessFile accfile;
	long pointer=0l;
//	Account aaccount;
		public AccountProcess()
		{
			this("",0);
			try
			{
				accfile=new RandomAccessFile("Account.atm","rw");
			}
			catch(IOException e)
			{
			
			}
		}
		public AccountProcess(String id,long amount)
		{
			super(id,amount);
			try
			{
				accfile=new RandomAccessFile("Account.atm","rw");
			}	
			catch(IOException e)
			{
			}
		}
		public void writeRecord() throws IOException
		{
			accfile.seek(accfile.length());
			writeFixId();
			accfile.writeLong(amount);
		}
		
		public void writeFixId()
		{
			StringBuffer buffer=null;
			buffer=new StringBuffer(custid);
			buffer.setLength(10);
			try
			{
				accfile.writeChars(buffer.toString());
			}
			catch(IOException e)
			{
				JOptionPane.showMessageDialog(null,"Error While Writing To The File");
			}
		}
		String readId() throws IOException
		{
			char id[]=new char[10],temp=' ';
			for(int i=0;i<id.length;i++)
			{
				try
				{

					temp=accfile.readChar();
				}

				catch(EOFException e)
				{
				
				}
				id[i]=temp;
			}

			return new String(id).replace('\0',' ');
		}
		
		public void readRecord(String id) throws IOException
		{
			accfile.seek(0l);
			pointer=0;
			while (pointer<=accfile.length())
			{
				try
				{
					String cid=readId();
					if(cid.trim().equals(id))
					{
						setId(cid);
						setAmount(accfile.readLong());
						return;
					}
					accfile.readLong();
					pointer+=28;
				}
				
				catch(EOFException e)
				{
				
				}
			}
		}
		public void deposit(String id,long amount) throws IOException
		{
		
			readRecord(id);
			accfile.seek(0l);
			pointer=0;
			while (pointer<=accfile.length())
			{
				try
				{
					String cid=readId();
					if(cid.trim().equals(id))
					{
						setId(cid);
						long prevamount=getAmount();
						long newamount=prevamount+amount;
						accfile.writeLong(newamount);
						return;
					}
					accfile.readLong();
					pointer+=28;
				}
				catch(EOFException e)
				{
				
				}
				
			}	
			
		}
		public void withdraw(String id,long amount) throws IOException
		{
			readRecord(id);
			accfile.seek(0l);
			pointer=0;
			while (pointer<=accfile.length())
			{
				try
				{
					String cid=readId();
					if(cid.trim().equals(id))
					{
						setId(cid);
						long newamount=getAmount()-amount;
						accfile.writeLong(newamount);
						return;
					}
					accfile.readLong();	
					pointer+=28;
				}
				catch(EOFException e)
				{
			
				}
			
			}	
		
		}
		
		
			
		
		
		
			
}		
	
class CustomerProcess extends Customer 
{

		private RandomAccessFile custfile;
		Vector records;
		long pointer=0l;
		Customer acustomer;
		boolean validid=false;
		public CustomerProcess()
		{
			this("","","","");
			try
			{
				custfile=new RandomAccessFile("Customer.atm","rw");
			}
			catch(IOException e)
			{
			}
		}
		public CustomerProcess(String id,String nam,String add,String pass )
		{
			super(id,nam,add,pass);
			records=new Vector();
			try
			{
				custfile=new RandomAccessFile("Customer.atm","rw");
			}
			catch(IOException e)
			{
			}
		}
		void writeRecord()throws IOException
		{
			custfile.seek(custfile.length());
			writeFixId();
			writeFixName();
			writeFixAddress();
			writeFixPassword();
		}
		void writeFixId()throws IOException
		{
			StringBuffer buffer=null;
			buffer=new StringBuffer(custid);
			buffer.setLength(10);
			custfile.writeChars(buffer.toString());
		}
		void writeFixName()throws IOException
		{
			StringBuffer buffer=null;
			buffer=new StringBuffer(name);
			buffer.setLength(20);
			custfile.writeChars(buffer.toString());
		}
		void writeFixAddress()throws IOException
		{
			StringBuffer buffer=null;
			buffer=new StringBuffer(address);
			buffer.setLength(25);
			custfile.writeChars(buffer.toString());
		}
		void writeFixPassword()throws IOException
		{
			StringBuffer buffer=null;
			buffer=new StringBuffer(password);
			buffer.setLength(10);
			custfile.writeChars(buffer.toString());
		}
			
		Vector readAll() throws IOException
		{

			
			custfile.seek(0l);

		while (pointer<=custfile.length())
			{

				try
				{	
					acustomer=new Customer(readId(),readName(),readAddress(),readPassword());
					records.addElement(acustomer);
					pointer+=130;
				}

				catch(EOFException e)
				{
					
					JOptionPane.showMessageDialog(null,"Error");
				}


			}
				
   		   return records;	
				
	 }
		String readId() throws IOException
		{
			char id[]=new char[10],temp=' ';
			for(int i=0;i<id.length;i++)
			{
				try
				{

					temp=custfile.readChar();
				}

				catch(EOFException e)
				{
				
				}
				id[i]=temp;
			}

			return new String(id).replace('\0',' ');
		}
		String readName()throws IOException
		{
			char n[]=new char[20],temp=' ';
			for(int i=0;i<n.length;i++)
			{
				try
				{
					temp=custfile.readChar();
				}
				catch(EOFException e)
				{
				
				}
				

				n[i]=temp;
			}

			return new String(n).replace('\0',' ');
		}
		String readAddress()throws IOException
		{
			char a[]=new char[25],temp=' ';
			for(int i=0;i<a.length;i++)
			{
				try
				{
					temp=custfile.readChar();
				}
				catch(EOFException e)
				{
				}
				a[i]=temp;
			}
					
			return new String(a).replace('\0',' ');
		}
		String readPassword()throws IOException
		{
			char p[]=new char[10],temp=' ';
			for(int i=0;i<p.length;i++)
			{
				try
				{
					temp=custfile.readChar();
				}
				catch(EOFException e)
				{
				}
				p[i]=temp;
			}
					
			return new String(p).replace('\0',' ');
		}
		public void closeFile() throws IOException
		{
			custfile.close();
		}
		
		public void readRecord(String id) throws IOException
		{
			custfile.seek(0l);
			pointer=0;
			while (pointer<=custfile.length())
	
			{
				try
				{
					String cid=readId();
					if(cid.trim().equals(id))
					{
						validid=true;
						setId(cid);
						setName(readName());
						setAddress(readAddress());
						setPassword(readPassword().trim());
						return;
					}
					readName();
					readAddress();
					readPassword();
					pointer+=130;
				}
				
				catch(EOFException e)
				{
				
				}
			}
			
		}		
		
		public boolean isValidID(String id) throws IOException
		{
			readRecord(id);
			if (validid) 
			{
				validid=false;
				return true; 
			}
			else
			return false;
		}	
		
		public void changePassword(String id, String newpas) throws IOException
		{
		
		
			custfile.seek(0l);
			pointer=0;
			while (pointer<=custfile.length())
	
			{
				try
				{
					String cid=readId();
					if(cid.trim().equals(id))
					{
						readName();
						readAddress();
						setPassword(newpas);
						writeFixPassword();
						return;
					}
					readName();
					readAddress();
					readPassword();
					pointer+=130;
				}
				
				catch(EOFException e)
				{
				
				}
			}
		
		
		
		
		}
				
		public static int customerRecordSize()
		{
			return 130;
		}
}	
class AuthorizationUI extends Panel 
{
	protected JTextField acno;
	protected JPasswordField pass;
	protected JButton adm,ok,cancel;
	protected JLabel acc,pas;
	protected JPanel gp,fp;
	public AuthorizationUI()
	{

		acno=new JTextField(10);
		pass=new JPasswordField(10);

		
		adm=new JButton("Administration");
		ok=new JButton("Ok");
		cancel=new JButton("Cancel");
		acc=new JLabel("Accoount No.");
		pas=new JLabel("Password");
		gp=new JPanel();
		fp=new JPanel();
		gp.setLayout(new GridLayout(2,2));
		gp.add(acc);
		gp.add(acno);
		gp.add(pas);
		gp.add(pass);
		fp.setLayout(new FlowLayout());
		fp.add(adm);
		fp.add(ok);
		fp.add(cancel);
		setLayout(new BorderLayout());
		add(gp,BorderLayout.NORTH);
		add(fp,BorderLayout.SOUTH);
		validate();
		
	}
	public JButton	getAdm()
	{
		return  adm;
	}	
	
	public JButton getOk()
	{
		return ok;
	}
	public JButton getCancel()
	{
		return cancel;
	}
	public JTextField getAcc()
	{
		return acno;
	}
	public JPasswordField getPass()
	{
		return pass;
	}	
		
}	
class AdmPass implements Serializable
{
	String password;
		
	public AdmPass(String pass)
	{
			password=pass;
	}
	
	public void setPassword(String pas)
	{
		password=pas;
	}
	public String getPasswrd()
	{
		return password;
	}
}		

class AdmAuthorize extends Dialog implements ActionListener
{
	ObjectInputStream admpassword;
	AdmPass pasobj;
	JLabel pas;
	JPasswordField password;
	JButton ok,cancel;
	JPanel uipanel,p1,p2;
	boolean accessgrant;

	
	
		public AdmAuthorize(Dialog parent,String title,boolean mode)
		{
			super(parent,title,mode);
			accessgrant=false;
			pas=new JLabel("Enter Password");
			password=new JPasswordField(10);
			ok=new JButton("Ok");
			cancel=new JButton("Cancel");
			uipanel=new JPanel();
			p1=new JPanel();
			p2=new JPanel();
			p1.setLayout(new FlowLayout());
			p1.add(pas);
			p1.add(password);
			p2.setLayout(new FlowLayout());
			p2.add(ok);
			p2.add(cancel);
			ok.addActionListener(this);
			uipanel.setLayout(new BorderLayout());
			uipanel.add(p1,BorderLayout.CENTER);
			uipanel.add(p2,BorderLayout.SOUTH);	
			add(uipanel,BorderLayout.CENTER);
			addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent we)
				{
					dispose();
				}
			});
			cancel.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent ce)
				{
					dispose();
				}
			});		
			setResizable(false);
			setLocation(350,300);
			setSize(340,120);
			show();

			
		}	
		public void actionPerformed(ActionEvent e)
		{
			
			if (e.getSource()==ok)
			{
					//	pasobj=new AdmPass("kris");
				try
				{
					//	ppp=new ObjectOutputStream(new FileOutputStream("Administrator.pas"));
					//	ppp.writeObject(pasobj);
						
					admpassword=new ObjectInputStream(new FileInputStream("Administrator.pas"));
					pasobj=(AdmPass)admpassword.readObject();	
						
				}	

				catch(ClassNotFoundException fe)
				{
					JOptionPane.showMessageDialog(null,"Unable to create object");
				}
				catch(FileNotFoundException fn)
				{
					JOptionPane.showMessageDialog(null,"File Dose Not Exist");
				}
				catch(IOException io)
				{
					JOptionPane.showMessageDialog(null,"Error Reading File");
				
				}
		
				String sa=pasobj.getPasswrd();
				String ip=new String(password.getPassword());
				if (ip.equals(sa))
				{
					JOptionPane.showMessageDialog(null,"Access Granted!");
					accessgrant=true;
					dispose();
			
				}
				else if(ip.equals(""))
				JOptionPane.showMessageDialog(null,"Enter Password");
				else if(!ip.equals(sa))
				JOptionPane.showMessageDialog(null,"Invalid Password");
			
				
			}		
			
		}
		
		public boolean isAccessGranted()
		{
			return accessgrant;
		}
		
		
}		
		

class Authorization extends Dialog implements ActionListener
{
	
	AuthorizationUI aui;
	AdmAuthorize admau;
	ObjectOutputStream ppp;
	JTextField accno;
	JPasswordField pass;
	CustomerProcess cust; 
	JButton adm,ok,cancel;
	String custid;
	boolean useradm=false,usercustomer=false;
	public Authorization(JFrame parent,String title,boolean mode)
	{
		super(parent,title,mode);
		aui=new AuthorizationUI();
		cust =new CustomerProcess();
		accno=aui.getAcc();
		pass=aui.getPass();
		adm=aui.getAdm();
		ok=aui.getOk();
		cancel=aui.getCancel();
		pass.addActionListener(this);
		ok.addActionListener( new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String id=new String(accno.getText());
				try
				{
					if (id.equals(""))
					JOptionPane.showMessageDialog(null,"Enter Account Number");	
				 	else if (cust.isValidID(id))
					{
						cust.readRecord(id);
						checkPassword();
					}
					else if(!cust.isValidID(id))
					JOptionPane.showMessageDialog(null,"This account number doesn't exist");	
								
				}
			
				
				catch(IOException fe)
				{
				}
					
				
			}
		});
				
						
		adm.addActionListener(this);										
		
		cancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}	
		});
		
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent we)
			{
				
				System.exit(0);
			}
		});
	
		
		add(aui,BorderLayout.CENTER);
		setResizable(false);
		setLocation(350,280);
		setSize(330,120);
		show();
	}	
	public void actionPerformed(ActionEvent e)
	{
	
	
		String id=accno.getText();	
		if (e.getSource()==pass)
		{
		try
		{
			if (id.equals(""))
			JOptionPane.showMessageDialog(null,"Enter Account Number");	
			else if (cust.isValidID(accno.getText()))
			{
				cust.readRecord(id);
				checkPassword();
			}
			else if(!cust.isValidID(id))
			JOptionPane.showMessageDialog(null,"This account number doesn't exist");	
		}
		catch(IOException fe)
		{
		}
		}
		if (e.getSource()==adm)
		{
			admau=new AdmAuthorize(this,"Administration",true);
			if (admau.isAccessGranted())
			{						
				useradm=true;
				dispose();
			}	
			
		}	

	}	
	public void checkPassword()
	{
							
				String custpas=cust.getPasswrd();
				String ip=new String(pass.getPassword());
				if (custpas.equals(ip))
				{
					JOptionPane.showMessageDialog(null,"Account Accessed");
					custid=new String(accno.getText());
					usercustomer=true;
					dispose();
				}	
				else
				JOptionPane.showMessageDialog(null,"Invalid Password Entered");	
	}			
	
	public boolean admEntered()
	{
		if(useradm)
			return true;
		else
			return false;
	}			
	
	public boolean customerEntered()
	{
		if(usercustomer)
			return true;
		else
			return false;
	}					
	public String getCustomerId()
	{
		if(usercustomer)
		return custid;
		else	
		return "";
		
	}	
			
}	

class DisplayCustomer extends JFrame implements ActionListener
{
	JTable view;
	Vector records;
	CustomerProcess p,cust;
	Container c;
	JScrollPane jp;
	JTextField t,t1,t2,t3;
	TextArea ta;
	JButton b;
	Enumeration e;
	AuthorizationUI aui;

	
	public 	DisplayCustomer() throws IOException
	{
		super("ATM");	
		c=getContentPane();
		t=new JTextField(10);
		t1=new JTextField(10);
		t2=new JTextField(10);
		t3=new JTextField(10);
		ta=new TextArea(30,0);
		b=new JButton("Authorize");
		records=new Vector(0);
		p=new CustomerProcess();
		cust=new CustomerProcess();
		records=p.readAll();
		e=records.elements();
		while (e.hasMoreElements())
		{
		
			ta.append(e.nextElement().toString());
		}
		cust.readRecord("A-444");
		t.setText(cust.toString());
		t1.setText(records.elementAt(1).toString());
		t2.setText(records.elementAt(2).toString());
		t3.setText(records.elementAt(3).toString());
		c.setLayout(new FlowLayout());
		c.add(t);
		c.add(t1);
		c.add(t2);
		c.add(t3);
		c.add(b);
		c.add(ta,BorderLayout.SOUTH);
		b.addActionListener(this);
		setSize(500,500);
		show();	repaint();
	}
	public void actionPerformed(ActionEvent e)
	{
	
		
	}	
		
	public void paint(Graphics g)
	{
		g.setColor(Color.blue);
		g.drawString(p.getName(),200,200);
	}
	

}

class AddNewAccount extends Dialog implements ActionListener
{
	private JTextField accnof,namef,addressf,amountf;
	private JPasswordField passwordf;
	private JLabel acno,nam,add,amt,pass;
	private JButton create,cancel;
	private JPanel uipanel;
	private AccountProcess account;
	private CustomerProcess customer;
	
	private long amount;
		public AddNewAccount(JFrame parent,String title,boolean mode)
		{
			super(parent,title,mode);
			acno=new JLabel("Account Number:");
			nam=new JLabel("Name:");
			add=new JLabel("Adddress");
			amt=new JLabel("Deposited Amount:");
			pass=new JLabel("Passoword");
			accnof=new JTextField(15);
			namef=new JTextField(15);
			addressf=new JTextField(15);
			amountf=new JTextField(15);
			passwordf=new JPasswordField(15);
			create=new JButton("Create");
			cancel=new JButton("Cancel");
			uipanel=new JPanel();
			customer=new CustomerProcess();
			uipanel.setLayout(new GridLayout(6,2));
			uipanel.add(acno);
			uipanel.add(accnof);
			uipanel.add(nam);
			uipanel.add(namef);
			uipanel.add(add);
			uipanel.add(addressf);
			uipanel.add(amt);
			uipanel.add(amountf);
			uipanel.add(pass);
			uipanel.add(passwordf);
			uipanel.add(create);
			uipanel.add(cancel);
			setLayout(new BorderLayout());
			add(uipanel,BorderLayout.CENTER);
			create.addActionListener(this);
			cancel.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					dispose();
				}
			});	
			addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent we)
				{
					dispose();
				}
			});		
			setResizable(false);
			setLocation(370,250);
			setSize(300,185);
			show();
		
						
		}	
			
		public void actionPerformed(ActionEvent e)
		{
			
			String ian=accnof.getText();
			String iamt=amountf.getText();
			String inam=namef.getText();
			String iadd=addressf.getText();
			String ipas=new String(passwordf.getPassword());
			boolean alreadyexist=false;
			
			try
			{
				alreadyexist=customer.isValidID(ian);
			}
			catch(IOException fe)
			{
				JOptionPane.showMessageDialog(null,"Fiel Error \n customer.atm");
				dispose();
			
			}	
				
			if(ian.equals(""))
			JOptionPane.showMessageDialog(null,"Account number must be Entered");
			if(inam.equals(""))
				JOptionPane.showMessageDialog(null,"Customer name is missing");
			if(ipas.equals(""))
				JOptionPane.showMessageDialog(null,"Customer must have password");
			if(alreadyexist&&!(ian.equals("")))	
				JOptionPane.showMessageDialog(null,"The account number alreay exist \n Eneter unique account number");
			if (!(ian.equals(""))&&!(inam.equals(""))&&!(ipas.equals(""))&&!alreadyexist)
			{		
				long amount;
				String amtst=amountf.getText();
				if(amtst.equals(""))
					amount=0;
				else	
					amount=Long.parseLong(amtst);

				account=new AccountProcess(ian,amount);
				customer =new CustomerProcess(ian,inam,iadd,ipas);
			try
				{
				
					account.writeRecord();
					customer.writeRecord();
					JOptionPane.showMessageDialog(null,"New account was Created");
					dispose();
				}
			catch(IOException fe)
			{
					JOptionPane.showMessageDialog(null,"Cannot create account /n File Error");
					dispose();
			}
			
			}
				
			
				
		}	
			
			
			
}		

class ChangeAdmPassword extends Dialog implements ActionListener
{
	JPasswordField prevpassf,currpassf;
	JLabel pp,np; 
	JButton change,cancel;
	JPanel uipanel;
	ObjectOutputStream  newadmpas;
	ObjectInputStream prevadmpas;
	String admpassword;
	AdmPass passobj;
	
	public 	ChangeAdmPassword(JFrame parent,String title,boolean mode)
	{
		super(parent,title,mode);
		try
		{
			prevadmpas=new ObjectInputStream(new FileInputStream("Administrator.pas"));
			passobj=(AdmPass)prevadmpas.readObject();
		}
		catch(ClassNotFoundException fe)
		{
			JOptionPane.showMessageDialog(null,"Unable to create object");
		}
		catch(FileNotFoundException fn)
		{
			JOptionPane.showMessageDialog(null,"File Dose Not Exist");
		}
		catch(IOException io)
		{
			JOptionPane.showMessageDialog(null,"Error Reading File");
		
		}
		admpassword=passobj.getPasswrd();
		prevpassf=new JPasswordField(10);
		currpassf=new JPasswordField(10);
		change=new JButton("Change");
		cancel=new JButton("Cancel");
		pp=new JLabel("Previous Password:");
		np=new JLabel("New Password:");
		uipanel=new JPanel();
		uipanel.setLayout(new GridLayout(3,2));
		uipanel.add(pp);
		uipanel.add(prevpassf);
		uipanel.add(np);
		uipanel.add(currpassf);
		uipanel.add(change);
		uipanel.add(cancel);
		setLayout(new BorderLayout());
		add(uipanel,BorderLayout.CENTER);
		addWindowListener(new WindowAdapter()
		{
			public void WindowClosing(WindowEvent we)
			{
				dispose();
			}
		});	
		cancel.addActionListener(this);
		change.addActionListener(this);
		setResizable(false);
		setLocation(350,250);
		setSize(250,100);
		show();
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==cancel)
			dispose();
		if (e.getSource()==change)
		{
			String np=new String(currpassf.getPassword());
			String ip=new String(prevpassf.getPassword());
			if (ip.equals(admpassword))
			{	
				if (np.equals(""))
				JOptionPane.showMessageDialog(null,"Enter New Passowrd");										
				else
				{
				
					try
					{
						newadmpas=new ObjectOutputStream(new FileOutputStream("Administrator.pas",false));
						passobj=new AdmPass(np);
						newadmpas.writeObject(passobj);
						JOptionPane.showMessageDialog(null,"Your password has been changed");														
						dispose();
					}
					catch(IOException fe)
					{
					}
				}	
			
			}
			else
			JOptionPane.showMessageDialog(null,"Invalid password encountered");								
		}	
			
	
	
	}
	
	
}	

class Deposit extends Dialog implements ActionListener
{
	JLabel amtl;
	JPanel uipanel,p;
	JButton deposit,cancel;
	JTextField amtf;
	AccountProcess acc;
	String id;
	
	public Deposit(JFrame parent,String title,boolean mode,String id)
	{
		super(parent,title,mode);
		this.id=id;		
		amtl=new JLabel("Enter amount to be deposited:");
		uipanel=new JPanel();
		p=new JPanel();
		deposit=new JButton("Deposit");
		cancel=new JButton("Cancel");
		amtf=new JTextField(10);
		acc= new AccountProcess();
		uipanel.setLayout(new GridLayout(1,1));
		uipanel.add(amtl);
		uipanel.add(amtf);
		p.setLayout(new FlowLayout());
		p.add(deposit);
		p.add(cancel);
		deposit.addActionListener(this);
		cancel.addActionListener(this);
		setLayout(new BorderLayout());
		add(uipanel,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent we)
			{
				dispose();
			}
		});		
		
		setResizable(false);
		setLocation(300,250);
		setSize(350,95);
		show();
	}	
	
	public void actionPerformed(ActionEvent ag)
	{
		if(ag.getSource()==cancel)
		dispose();		
		if (ag.getSource()==deposit)
		{
			long amount=0l;
			try
			{
				amount=Long.parseLong(amtf.getText());
			}
			catch(NumberFormatException ee)
			{
				JOptionPane.showMessageDialog(null,"Enter Numerical Value");
			}	
			try
			{
				acc.deposit(id,amount);
				JOptionPane.showMessageDialog(null,"The amount has been deposited!");
				dispose();
			}
			catch(IOException e)
			{
				JOptionPane.showMessageDialog(null,"File error");
				dispose();
			}
		}		
			
			
	}	
}

class Withdraw extends Dialog implements ActionListener
{
	JLabel amtl;
	JPanel uipanel,p;
	JButton deposit,cancel;
	JTextField amtf;
	AccountProcess acc;
	String id;
	
	public Withdraw(JFrame parent,String title,boolean mode,String id)
	{
		super(parent,title,mode);
		this.id=id;		
		amtl=new JLabel("Enter amount to be Withdrawn:");
		uipanel=new JPanel();
		p=new JPanel();
		deposit=new JButton("Withdraw");
		cancel=new JButton("Cancel");
		amtf=new JTextField(10);
		acc= new AccountProcess();
		uipanel.setLayout(new GridLayout(1,1));
		uipanel.add(amtl);
		uipanel.add(amtf);
		p.setLayout(new FlowLayout());
		p.add(deposit);
		p.add(cancel);
		deposit.addActionListener(this);
		cancel.addActionListener(this);
		setLayout(new BorderLayout());
		add(uipanel,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent we)
			{
				dispose();
			}
		});		
		
		setResizable(false);
		setLocation(300,250);
		setSize(350,95);
		show();
	}	
	
	public void actionPerformed(ActionEvent ag)
	{
		if(ag.getSource()==cancel)
		dispose();		
		if (ag.getSource()==deposit)
		{
			long amount=0l;
			String amt=amtf.getText();
			
			if (amt.equals(""))
			{
					JOptionPane.showMessageDialog(null,"Enter amount to be Withdrawn.");
					
			}	
			else
			 {
			 		try
						{
							amount=Long.parseLong(amtf.getText());
						//	acc.setA
						}
						
					catch(NumberFormatException ee)
					{
						JOptionPane.showMessageDialog(null,"Enter Numerical Value");
					}	
					try
				{
					acc.withdraw(id,amount);
					JOptionPane.showMessageDialog(null,"Your amount has been withdrawn!");
					dispose();
							
				}
					catch(IOException e)
					{
						JOptionPane.showMessageDialog(null,"File error");
					}
			  }	
		}	
			
			
	}	
}

class Transfer extends Dialog implements ActionListener
{
	JLabel accnol,amtl;
	JTextField accnof,amtf;
	JButton transfer,cancel;
	AccountProcess account;
	CustomerProcess customer;
	JPanel uipanel,p;
	String id;
	
	public Transfer(JFrame parent,String title,boolean mode,String id)
	{
		super(parent,title,mode);
		this.id=id;
		accnol=new JLabel("Account number to transfer to:");
		amtl=new JLabel("Amount: to be transfered:");
		accnof=new JTextField(10);
		amtf=new JTextField(10);
		transfer=new JButton("Transfer");
		cancel=new JButton("Cancel");
		account=new AccountProcess();
		customer=new CustomerProcess();
		uipanel=new JPanel();
		p=new JPanel();
		uipanel.setLayout(new GridLayout(2,2));
		uipanel.add(accnol);
		uipanel.add(accnof);
		uipanel.add(amtl);
		uipanel.add(amtf);
		p.setLayout(new FlowLayout());
		p.add(transfer);
		p.add(cancel);
		setLayout(new BorderLayout());
		add(uipanel,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		cancel.addActionListener(this);
		transfer.addActionListener(this);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent we)
			{
				dispose();
			}
		});		
		
		setResizable(false);
		setLocation(360,200);
		setSize(350,120);
		show();
	}	
	
	public void actionPerformed(ActionEvent ag)
	{
		long amount=0l;
		boolean valid=false;
		if(ag.getSource()==cancel)
			dispose();
		if (ag.getSource()==transfer)
		{		
			String transferto=accnof.getText();
			if(transferto.equals(""))
			{
				JOptionPane.showMessageDialog(null,"Enter Account Number");							
			}
			else
			{		
				try
				{
					valid=customer.isValidID(transferto);
				}
				catch(IOException ioe)
				{
					JOptionPane.showMessageDialog(null,"Error,while reading from file");				
				}	
			
				if (valid)
				{
					String amt=amtf.getText();
					try
					{
						amount=Long.parseLong(amt);
					}
					catch(NumberFormatException nfe)
					{
						JOptionPane.showMessageDialog(null,"Enter Numerical Value");
						dispose();
					}
					try
					{
						account.deposit(transferto,amount);
						account.withdraw(id,amount);
						JOptionPane.showMessageDialog(null,"The amount has been transfered \n to the given account number");				
						dispose();
					}
					catch(IOException fe)	
					{
						JOptionPane.showMessageDialog(null,"Error,while reading from file");				
					}	
				}
				
				else 
				JOptionPane.showMessageDialog(null,"This account number does not exist");	
				
		
			}	

			
		}
	}		
			
				
}	

class ChangeCustomerPassword extends Dialog implements ActionListener
{

	JPasswordField prevpassf,currpassf;
	JLabel pp,np; 
	JButton change,cancel;
	JPanel uipanel,p;
	CustomerProcess customer;
	String id;

	public	ChangeCustomerPassword(JFrame parent,String title,boolean mode,String id)
	{
		super(parent,title,mode);
		this.id=id;
		prevpassf=new JPasswordField(10);
		currpassf=new JPasswordField(10);
		change=new JButton("Change");
		cancel=new JButton("Cancel");
		pp=new JLabel("Previous Password:");
		np=new JLabel("New Password:");
		uipanel=new JPanel();
		p=new JPanel();
		customer=new CustomerProcess();
		uipanel.setLayout(new GridLayout(2,2));
		uipanel.add(pp);
		uipanel.add(prevpassf);
		uipanel.add(np);
		uipanel.add(currpassf);
		p.setLayout(new FlowLayout());
		p.add(change);
		p.add(cancel);
		cancel.addActionListener(this);
		change.addActionListener(this);
		setLayout(new BorderLayout());
		add(uipanel,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		addWindowListener(new WindowAdapter()
		{
			public void WindowClosing(WindowEvent we)
			{
				dispose();
			}
		});	
		setResizable(false);
		setLocation(350,250);
		setSize(250,115);
		show();
	}
	
	public void actionPerformed(ActionEvent ag)
	{
		if(ag.getSource()==cancel)
			dispose();
		if (ag.getSource()==change)
		{
			String pp=new String(prevpassf.getPassword());
			if(pp.equals(""))
			JOptionPane.showMessageDialog(null,"Enter Previous Password");
			else 
			{
				try
				{
					customer.readRecord(id);
				}
				catch(IOException fe)
				{
				}
					
				String custpas=customer.getPasswrd();
				if (custpas.equals(pp))
				{
					String newpas=new String(currpassf.getPassword());
					if (newpas.equals(""))
					{
						JOptionPane.showMessageDialog(null,"Enter New Password");
					}	
					else
					{
						try
						{
							customer.changePassword(id,newpas);
							JOptionPane.showMessageDialog(null,"Your password has been changed!");
							dispose();
						}
						catch(IOException fee)
						{
						}
					}
			    }
				else
				JOptionPane.showMessageDialog(null,"Invalid Passoword Encountered");
			}
		}	
	}			
					
			
		
}		
		
	

class CustomerInquiry extends Dialog
{
	JTextField namef,idf,addf,amtf;
	JLabel namel,idl,addl,amtl;
	JButton ok;
	String id;
	JPanel uipanel,p;
	CustomerProcess cust; 
	AccountProcess acc;
	
	public CustomerInquiry(JFrame parent,String title,boolean mode,String id)
	{
		super(parent,title,mode);
		this.id=id;
		namef=new JTextField(15);
		idf=new JTextField(15);
		addf=new JTextField(15);
		amtf=new JTextField(15);
		namel=new JLabel("Name:");
		idl=new JLabel("Account Number:");
		addl=new JLabel("Address:");
		amtl=new JLabel("Balance Rs.");
		ok=new JButton("Done");
		namef.setBackground(Color.pink);
		idf.setBackground(Color.pink);
		addf.setBackground(Color.pink);
		amtf.setBackground(Color.pink);
		uipanel=new JPanel();
		p=new JPanel();
		idf.setEditable(false);
		namef.setEditable(false);
		addf.setEditable(false);
		amtf.setEditable(false);
		cust=new CustomerProcess();
		acc=new AccountProcess();
		p.setLayout(new FlowLayout());
		uipanel.setLayout(new GridLayout(4,2));
		uipanel.add(idl);
		uipanel.add(idf);
		uipanel.add(namel);
		uipanel.add(namef);
		uipanel.add(addl);
		uipanel.add(addf);
		uipanel.add(amtl);
		uipanel.add(amtf);
		p.add(ok);
		setLayout(new BorderLayout());
		add(uipanel,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		try
		{
			cust.readRecord(id);
			acc.readRecord(id);
		}
		catch(IOException fe)
		{
		
		}
		idf.setText(cust.getId());
		namef.setText(cust.getName());
		addf.setText(cust.getAddress());
		amtf.setText(String.valueOf(acc.getAmount()));
		ok.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				dispose();
			}
		});		
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent we)
			{
				dispose();
			}
		});		
		setResizable(false);
		setLocation(300,200);
		setSize(280,180);
		show();
	}	
}
 class Web extends JApplet
{
	URL necweb;
	
	public void init()
	{
		try
		{
			necweb= new URL("http//:www.nec.edu.org");
		}
		catch(MalformedURLException mue)
		{
		}
		AppletContext app=getAppletContext();
		app.showDocument(necweb);
	}	
}	
		
		
		
	

	
	

class About extends Dialog implements ActionListener
{	
	JLabel projectname,credits,college,programmer1,programmer2,programmer3;
	JApplet ja;
	Cursor webcursor;
	JButton ok;
	JPanel p1,p2,p3,p4,p5;
	long time=0;
	int mod,x,y,click;
	boolean tp=false;
	URL necweb;
	
	public About(JFrame parent,String title,boolean mode)
	{
		
		super(parent,title,mode);
		projectname=new JLabel(" Automated Bank Teller");
		projectname.setForeground(Color.red);
		projectname.setFont(new Font("Serif",Font.BOLD,36));
		credits=new JLabel("Credits:");
		credits.setFont(new Font("Serif",Font.BOLD+Font.ITALIC,20));
		programmer1=new JLabel("Krishna Sapkota(02-529) Lead Programmer");
		programmer1.setForeground(Color.blue);
		programmer1.setFont(new Font("Monospaced",Font.PLAIN,15));
		programmer2=new JLabel("Ronesh Chitrakar(02-517)");
		programmer2.setForeground(Color.blue);
		programmer2.setFont(new Font("Monospaced",Font.PLAIN,15));
		programmer3=new JLabel("Prashant Aryal(02-533)");
		programmer3.setForeground(Color.blue);
		programmer3.setFont(new Font("Monospaced",Font.PLAIN,15));
		college=new JLabel("         Nepal Engineering College");
		college.setForeground(Color.magenta);
		college.setFont(new Font("Serif",Font.BOLD,24));
				p1=new JPanel();
		p2=new JPanel();
		p3=new JPanel();
		p4=new JPanel();
		p5=new JPanel();
		ok=new JButton("Ok");
		webcursor=new Cursor(Cursor.HAND_CURSOR);
		ok.setBackground(Color.yellow);
		ok.setForeground(Color.blue);
		setLayout(new BorderLayout());
		add(projectname,BorderLayout.NORTH);
		p1.setLayout(new GridLayout(4,1));
		p3.setLayout(new GridLayout(2,1));
		p5.setLayout(new GridLayout(2,1,0,0));
		p2.add(credits);
		p2.add(programmer1);
		p2.add(programmer2);
		p2.add(programmer3);
		p3.add(college);
		p4.add(ok);
		p5.add(p3);
		p5.add(p4);
		add(p2,BorderLayout.CENTER);
		add(p5,BorderLayout.SOUTH);
		ok.addActionListener(this);
			addWindowListener(new WindowAdapter()
		{
			public void WindowClosing(WindowEvent e)
			{
				dispose();
			}
		});
		college.addMouseListener(new MouseAdapter()
		{
			public void mouseEntered(MouseEvent e)
			{
				setCursor(webcursor);
				college.setForeground(Color.blue);
			}
			public void mouseExited(MouseEvent e)
			{
				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				college.setForeground(Color.magenta);
			}	
			public void mouseClicked(MouseEvent e)
			{
			
				JOptionPane.showMessageDialog(null,"You Are Not Connected to Internet");
			
//				web.show();
			}	
			
			
			
		});	
	/*	addMouseListener(new MouseMotionAdapter()
		{
			public void mouseMoved(MouseEvent e)
			{
		
		
			}
		});*/
		
	
		setResizable(false);
		setLocation(300,200);
		setSize(380,330);
		show();
	}
	public void actionPerformed(ActionEvent ag)
	{
		dispose();
	}
	public JLabel getCollegeLabel()
	{
		return college;
	} 
	
	
		
}		


		
		
		
		
		
		
	
	
	
		
		
	
		
 class NewAtm extends JFrame implements ActionListener
{
		Authorization protect;
		ChangeAdmPassword chngadmpas;
		ChangeCustomerPassword ccp;
		Deposit dp;
		Withdraw wd;
		Transfer tr;
		AddNewAccount na;
		CustomerInquiry inq;
		About ab;
		private UIManager.LookAndFeelInfo looks[];
		JMenuBar bar;
		JMenu account,help,password;
		JMenuItem chngpas,contents,about,createnew,modify,delete;

		JMenu transaction,custpassword,custhelp,inquiry;
		JMenuItem custchngpas,custcontent,custabout,deposit,withdrawl,transfer,viewinq;
		
		NewAtm()
		{
	
			looks=UIManager.getInstalledLookAndFeels();	
			try
			{
				UIManager.setLookAndFeel(looks[2].getClassName());
				
				SwingUtilities.updateComponentTreeUI(this);
			}
			catch(Exception e)
			{
			}
				
			
			protect=new Authorization(this,"Authorization",true);
			if (protect.admEntered())
			{
				setTitle("ATM, Accessed by Administrator");				
				bar=new JMenuBar();
				setJMenuBar(bar);
				account=new JMenu("Account");
				help=new JMenu("Help");
				password=new JMenu("Password");
				chngpas=new JMenuItem("Change Password");
				chngpas.setMnemonic('C');
				password.setMnemonic('P');
				password.add(chngpas);
				contents=new JMenuItem("Contents");
				about=new JMenuItem("About");
				about.setMnemonic('A');
				contents.setMnemonic('C');
				help.setMnemonic('H');
				help.add(contents);
				help.add(about);
				account.setMnemonic('A');
				createnew=new JMenuItem("Create new");
				createnew.setMnemonic('C');
				modify=new JMenuItem("Modify");
				modify.setMnemonic('M');
				delete=new JMenuItem("Delete");
				delete.setMnemonic('D');
				account.add(createnew);
				account.add(modify);
				account.add(delete);
				bar.add(account);
				bar.add(password);
				bar.add(help);
				createnew.addActionListener(this);
				chngpas.addActionListener(this);		
			}
			if (protect.customerEntered())
			{	
				setTitle("ATM, Accessed by Customer");				
				JMenuBar custbar=new JMenuBar();
				setJMenuBar(custbar);
				transaction=new JMenu("Transaction");
				transaction.setMnemonic('T');
				inquiry=new JMenu("Inquiry");
				inquiry.setMnemonic('I');
				viewinq=new JMenuItem("View Report");
				viewinq.setMnemonic('V');
				custpassword=new JMenu("Passoword");
				custpassword.setMnemonic('P');
				custchngpas=new JMenuItem("Change Password");
				custchngpas.setMnemonic('C');
				custhelp=new JMenu("Help");
				custhelp.setMnemonic('H');
				custcontent=new JMenuItem("Contents");
				custcontent.setMnemonic('C');
				custabout=new JMenuItem("About");
				custabout.setMnemonic('A');
				custhelp.add(custcontent);
				custhelp.add(custabout);
				deposit=new JMenuItem("Deposit");
				deposit.setMnemonic('D');
				withdrawl=new JMenuItem("Withdraw");
				withdrawl.setMnemonic('W');
				transfer=new JMenuItem("Transfer");
				transfer.setMnemonic('T');
				transaction.add(deposit);
				transaction.add(withdrawl);
				transaction.add(transfer);
				custpassword.add(custchngpas);
				inquiry.add(viewinq);
				
				custbar.add(transaction);
				custbar.add(inquiry);
				custbar.add(custpassword);
				custbar.add(custhelp);
				viewinq.addActionListener(this);
				deposit.addActionListener(this);
				withdrawl.addActionListener(this);
				transfer.addActionListener(this);
				custchngpas.addActionListener(this);
				custabout.addActionListener(this);
			}	
				
			addWindowListener(new WindowAdapter()
			{
				public void WindowClosing(WindowEvent e)
				{
					System.exit(0);
				}
			});
			setSize(1024,800);	
			setVisible(true);	
			
		}		
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==createnew)
			na=new AddNewAccount(this,"New Account",false);
			if (e.getSource()==chngpas)
			{
				try
				{
					chngadmpas=new ChangeAdmPassword(this,"Change Password",true);
				}
				catch(NullPointerException npe)
				{
					JOptionPane.showMessageDialog(null,"Exception Caught");
				
				}	
			}	
			if (e.getSource()==viewinq)
			{
				inq= new CustomerInquiry(this,"Inquiry",true,protect.getCustomerId());
			}
			if(e.getSource()==deposit)
				dp=new Deposit(this,"Deposit",false,protect.getCustomerId());
			if(e.getSource()==withdrawl)
				wd=new Withdraw(this,"Withdrawl",false,protect.getCustomerId());
			if(e.getSource()==transfer)
				tr=new Transfer(this,"Transfer Amount",false,protect.getCustomerId());	
			if(e.getSource()==custchngpas)
				ccp=new ChangeCustomerPassword(this,"Change Passoword",true,protect.getCustomerId());	
			if (e.getSource()==custabout)
			{
					ab=	new About(this,"About",true);
			}
	
			
					
			

		}

			
			
	
	public static void main(String arg[])throws IOException
	{

		NewAtm atm=new NewAtm() ;
	
	}

	
}
		
		
		
		
			
		
		
		
